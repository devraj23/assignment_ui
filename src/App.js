import "./App.css";
import { Footer } from "./component/Footer";
import { Home } from "./component/Home";
import { NavBar } from "./component/Navbar";

function App() {
  return (
    <div>
      <NavBar />
      <Home />
      <Footer />
    </div>
  );
}

export default App;
