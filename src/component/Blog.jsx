import { Col, Container, Row } from "react-bootstrap";
import { EllipsisText, Primary, SubTitle } from "./StyleComponent";

const blogs = [
  {
    id: "1",
    title: "Where does it come from?",
    info: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.',
    date: "10 may 2022",
    aurthor: "Admin",
    image: require("../assets/images/Roast.jpg"),
  },
  {
    id: "1",
    title: "Where does it come from?",
    info: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.',
    date: "10 may 2022",
    aurthor: "Admin",
    image: require("../assets/images/Roast.jpg"),
  },
  {
    id: "1",
    title: "Where does it come from?",
    info: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.',
    date: "10 may 2022",
    aurthor: "Admin",
    image: require("../assets/images/Roast.jpg"),
  },
  {
    id: "1",
    title: "Where does it come from?",
    info: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.',
    date: "10 may 2022",
    aurthor: "Admin",
    image: require("../assets/images/Roast.jpg"),
  },
];

export const Blogs = () => {
  return (
    <Container className="py-5">
      <Row>
        {blogs.map((blog) => (
          <Col style={{ minWidth: "320px", maxWidth: "320px" }}>
            <div className="text-center">
              <img
                src={blog.image}
                alt=""
                width="100%"
                height="200px"
                style={{ borderRadius: "16px", objectFit: "cover" }}
              />
              <div className="p-3">
                <EllipsisText
                  lineClamp="1"
                  fontWeight="600 "
                  fontSize="18px"
                  color="#4c3100"
                >
                  {blog.title}
                </EllipsisText>

                <EllipsisText lineClamp="3">{blog.info}</EllipsisText>
                <div className="mt-1">
                  <Primary fontWeight="600">{blog.aurthor}</Primary>
                  <Primary fontWeight="600">{blog.date}</Primary>
                </div>
              </div>
            </div>
          </Col>
        ))}
      </Row>
    </Container>
  );
};
