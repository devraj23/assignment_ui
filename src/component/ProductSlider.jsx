import React from "react";
import { SubTitle } from "./StyleComponent";
import { Container } from "react-bootstrap";
import Slider from "react-slick";

export default function ProductSlider({ product }) {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 6,
    slidesToScroll: 1,
  };
  return (
    <Container>
      <Slider {...settings} centerMode={true}>
        {product.map((food) => (
          <div className=" text-center">
            <img
              src={food.image}
              alt=""
              width="150px"
              height="150px"
              style={{ borderRadius: "12px" }}
            />
            <SubTitle className="mt-2" style={{ width: "150px" }}>
              {food.name}
            </SubTitle>
          </div>
        ))}
      </Slider>
    </Container>
  );
}
