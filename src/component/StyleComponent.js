import styled from "styled-components";
import { Button, Nav as Navs } from "react-bootstrap";

export const Nav = styled(Navs)`
  align-items: center;
  justify-content: space-between;
  width: 70%;
  margin-right: 30px;
  @media (max-width: 992px) {
    position: absolute;
    top: 67px;
    background: #e0eeec;
    z-index: 10;
    width: 100%;
    right: 0;
  }
`;

export const Line = styled.span`
  display: block;
  width: 30px;
  height: 3px;
  background: black;
  margin-block: 10px;
  border-radius: 4px;
  transition: transform 0.25s;
  opacity: ${(props) => props.opacity ?? "0.25"};
  transform: ${(props) => props.transform ?? "translateY(0px) rotate(0deg)"};
`;

export const Input = styled.input`
  border: none;
  box-sizing: border-box;
  border-radius: 16px;
  padding: 4px 16px 4px 40px;
  width: 100%;
  min-width: 120px;
  outline: none;
`;

export const CustomLink = styled(Navs.Link)`
  color: ${(props) => props.currentnav ?? "#74B750"};
  text-decoration: none;
  height: 35px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
  font-weight: 600;
  &:hover {
    color: ${(props) => props.color ?? "#74B750"};
  }
`;

export const CheckBoxWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const CheckBoxLabel = styled.label`
  position: absolute;
  top: 0;
  left: 0;
  width: 42px;
  height: 26px;
  border-radius: 15px;
  background: #bebebe;
  cursor: pointer;
  &::after {
    content: "";
    display: block;
    border-radius: 50%;
    width: 18px;
    height: 18px;
    margin: 3px;
    background: #ffffff;
    box-shadow: 1px 3px 3px 1px rgba(0, 0, 0, 0.2);
    transition: 0.2s;
  }
`;
export const CheckBox = styled.input`
  opacity: 0;
  z-index: 1;
  border-radius: 15px;
  width: 42px;
  height: 26px;
  &:checked + ${CheckBoxLabel} {
    background: #4fbe79;
    &::after {
      content: "";
      display: block;
      border-radius: 50%;
      width: 18px;
      height: 18px;
      margin-left: 21px;
      transition: 0.2s;
    }
  }
`;

export const NavBox = styled.div.attrs(() => ({
  className:
    "d-flex justify-content-center align-items-center position-relative me-2",
}))`
  width: 40px;
  height: 40px;
  border-radius: 4px;
  border: 1px solid #000;
`;

export const Trapezoid = styled.div`
  border-bottom: 600px solid #eabc04;
  border-left: 325px solid transparent;
  border-right: 0px solid transparent;
  height: 0;
  width: 52vw;
  position: absolute;
  top: 50px;
  z-index: -1;
  right: 0px;
`;

export const Title = styled.h1`
  color: #006600;
`;

export const SubTitle = styled.h6`
  color: ${(props) => props.color ?? " #d2a903"};
`;

export const Primary = styled.p`
  color: ${(props) => props.color ?? "#171200"};
  margin: 0px;
  font-weight: ${(props) => props.fontWeight};
`;

export const BoldTitle = styled.h3`
  color: ${(props) => props.color ?? " #d2a903"};
  font-size: ${(props) => props.fontSize ?? "20px"};
  font-weight: ${(props) => props.fontWeight ?? "800"};
`;

export const CustomButton = styled(Button)`
  color: ${(props) => props.color ?? "rgb(99, 1, 28)"};
  border-radius: ${(props) => props.borderRadius ?? "24px"};
  background: ${(props) => props.background ?? "rgb(255, 255, 255)"};
  border: 1px solid rgb(99, 1, 28);
  &:hover {
    background: auto;
  }
  &:active {
    background: auto;
  }
  &:focus {
    background: auto;
  }
`;

export const SkewDiv = styled.div`
  position: relative;
  z-index: 3;
  &:before {
    content: "";
    position: absolute;
    left: -10px;
    width: 110%;
    height: 50px;
    top: -22px;
    z-index: -1;
    background: white;
    transform: rotate(6deg);
  }
`;

export const EllipsisText = styled.p`
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: ${(props) => props.lineClamp ?? "1"};
  -webkit-box-orient: vertical;
  margin: 0px;
  font-size: ${(props) => props.fontSize ?? "14px"};
  color: ${(props) => props.color ?? "#000000"};
  font-weight: ${(props) => props.fontWeight ?? "normal"};
  width: ${(props) => props.width ?? "100%"};
`;

export const FeatureBox = styled.div.attrs(() => ({
  className: "d-flex justify-content-center align-items-center",
}))`
  width: 80px;
  height: 80px;
  border: 2px solid rgb(255, 255, 255);
  border-radius: 80px;
  background: rgb(237, 245, 245);
`;

export const Logo = styled.h1`
  font-size: 72px;
  background: -webkit-linear-gradient(#eee, #333);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;
