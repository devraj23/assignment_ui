import { useState } from "react";
import { Col, Container, Navbar, Row } from "react-bootstrap";
import { Cart, Location, Search, User } from "../assets/Icon";
import {
  CheckBox,
  CheckBoxLabel,
  CheckBoxWrapper,
  CustomLink,
  Input,
  Line,
  Logo,
  Nav,
  NavBox,
} from "./StyleComponent";
import "./toggle.css";

export const NavBar = () => {
  const [expanded, setExpanded] = useState(false);

  return (
    <>
      <div
        style={{
          background: "#198c19",
        }}
      >
        <Container>
          <Row
            className=" justify-content-between align-items-center"
            style={{ minHeight: "50px" }}
          >
            <Col className="d-flex" sm>
              <div className="d-flex justify-content-between align-items-center">
                <p className="m-0 me-4 d-none d-sm-block text-white">
                  9843561293
                </p>
              </div>

              <div className="d-flex justify-content-between align-items-center">
                <p className="m-0 d-none d-sm-block text-white">9843561293</p>
              </div>
            </Col>
            <Col
              sm
              className="position-relative d-flex justify-content-center align-items-center"
            >
              <Location />
              <Input placeholder="Chose Location" />
            </Col>
            <Col sm className="text-end">
              <p className="m-0 d-none d-sm-block text-white">
                Currently We Are
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <Navbar
        expanded={expanded}
        expand="lg"
        id="stickyNavBar"
        className="w-100 p-0  "
        style={{ height: "85px", zIndex: "999" }}
      >
        <Container className="">
          <Navbar.Brand className="p-0 c-pointer">
            <Logo>LOGO</Logo>
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="responsive-navbar-nav"
            onClick={() => setExpanded(expanded ? false : "expanded")}
          >
            <Line
              transform={
                expanded === "expanded"
                  ? "translateY(13px) rotate(45deg)"
                  : "translateY(0px) rotate(0deg)"
              }
            />
            <Line opacity={expanded === "expanded" ? "0" : "0.25"} />
            <Line
              transform={
                expanded === "expanded"
                  ? "translateY(-13px) rotate(-45deg)"
                  : "translateY(0px) rotate(0deg)"
              }
            />
          </Navbar.Toggle>
          <Navbar.Collapse
            id="responsive-navbar-nav"
            className="justify-content-end position-relative"
          >
            <Nav>
              <CheckBoxWrapper>
                <CustomLink>Suprise Food</CustomLink>
                <div className="position-relative d-flex">
                  <CheckBox id="checkbox" type="checkbox" />
                  <CheckBoxLabel htmlFor="checkbox" />
                </div>
              </CheckBoxWrapper>
              <CustomLink>Home</CustomLink>
              <CustomLink>Food</CustomLink>
              <CustomLink>Beverages</CustomLink>
              <CustomLink>Cake</CustomLink>
              <CustomLink>Healthy diet</CustomLink>
            </Nav>
            <div className="d-flex">
              <NavBox>
                <User />
              </NavBox>
              <NavBox>
                <Search />
              </NavBox>
              <NavBox>
                <Cart />
                <span
                  style={{
                    position: "absolute",
                    top: "-6px",
                    right: "-5px",
                    width: "15px",
                    height: "15px",
                    borderRadius: "15px",
                    background: "green",
                    zIndex: "2",
                    fontSize: "10px",
                    color: "#fff",
                    textAlign: "center",
                  }}
                >
                  2
                </span>
              </NavBox>
            </div>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};
