import { Button, Col, Container, Row } from "react-bootstrap";
import { AppStore, Cart, DiscountCard, PlayStore, Star } from "../assets/Icon";
import { Blogs } from "./Blog";
import { OurStrength } from "./OurStrength";
import Slider from "./ProductSlider";
import {
  BoldTitle,
  CustomButton,
  EllipsisText,
  FeatureBox,
  Primary,
  SkewDiv,
  SubTitle,
  Title,
  Trapezoid,
} from "./StyleComponent";
import { Video } from "./VideoPlayer";

const FoodItems = [
  {
    id: "1",
    name: "Roast",
    image: require("../assets/images/Roast.jpg"),
    rating: 8,
    price: "600",
    detail: " Remaining Essential Remaining Essential Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "2",
    name: "Rice",
    image: require("../assets/images/rice.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "3",
    name: "Kfc",
    image: require("../assets/images/kfc.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "4",
    name: "Burger",
    image: require("../assets/images/burger.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "5",
    name: "Pizza",
    image: require("../assets/images/pizza.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "4",
    name: "Burger",
    image: require("../assets/images/burger.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "5",
    name: "Pizza",
    image: require("../assets/images/pizza.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "4",
    name: "Burger",
    image: require("../assets/images/burger.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "5",
    name: "Pizza",
    image: require("../assets/images/pizza.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "4",
    name: "Burger",
    image: require("../assets/images/burger.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "5",
    name: "Pizza",
    image: require("../assets/images/pizza.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "4",
    name: "Burger",
    image: require("../assets/images/burger.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
  {
    id: "5",
    name: "Pizza",
    image: require("../assets/images/pizza.jpg"),
    rating: 8,
    price: "600",
    detail: "Remaining Essential",
    popularity: "Trending This Week",
  },
];

export const Home = () => {
  return (
    <>
      <div style={{ minHeight: "80vh" }}>
        <Container>
          <Row className="d-flex justify-content-center align-items-center ">
            <Col
              sm={5}
              className="d-flex justify-content-start align-items-center flex-column"
            >
              <Title className="w-100">Food On Ways</Title>
              <SubTitle className="w-100">Enjoys Your Food Any Time</SubTitle>
              <Primary>
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries,
              </Primary>
            </Col>
            <Col sm={6}>
              <img src={require("../assets/images/plate.png")} alt="" />
            </Col>
          </Row>
        </Container>
        <Trapezoid></Trapezoid>
      </div>
      <div>
        <Slider product={FoodItems} />
      </div>

      <Container className="py-5">
        <Row>
          <Col sm>
            <div
              className="p-4 px-5"
              style={{
                background: "linear-gradient(to bottom,#fa4e7d,#F0D04F )",
                borderRadius: "12px",
              }}
            >
              <Row className="d-flex justify-content-center align-items-center ">
                <Col sm>
                  <BoldTitle color="#fff">Flat 30% Off</BoldTitle>
                  <SubTitle color="#63011c">Festive Offer</SubTitle>
                  <BoldTitle color="#fff">Meal Combo Family Pack</BoldTitle>
                  <CustomButton className="mt-3">Grab It Now</CustomButton>
                </Col>
                <Col sm>
                  <img
                    src={require("../assets/images/plate.png")}
                    alt=""
                    width="200px"
                    height="250px"
                  />
                </Col>
              </Row>
            </div>
          </Col>
          <Col sm>
            <div
              className="p-4 px-5"
              style={{
                background: "linear-gradient(to bottom,#fa4e7d,#F0D04F )",
                borderRadius: "12px",
              }}
            >
              <Row className="d-flex justify-content-center align-items-center ">
                <Col sm>
                  <BoldTitle color="#fff">Flat 30% Off</BoldTitle>
                  <SubTitle color="#63011c">Festive Offer</SubTitle>
                  <BoldTitle color="#fff">Meal Combo Family Pack</BoldTitle>

                  <CustomButton className="mt-3">Grab It Now</CustomButton>
                </Col>
                <Col sm>
                  <img
                    src={require("../assets/images/plate.png")}
                    alt=""
                    width="200px"
                    height="250px"
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Container>
      <div className="text-center">
        <BoldTitle color="#4a0015">Best Selling Food</BoldTitle>
        <SubTitle>Choose Your food from Our Category</SubTitle>
      </div>
      <Container className=" py-5">
        <BestSelling />

        <div className="mt-4 d-flex justify-content-center">
          <CustomButton borderRadius="8px">
            <SubTitle className="m-0">See All</SubTitle>
          </CustomButton>
        </div>
      </Container>
      <div style={{ background: "#ffe4b2" }} className="py-5">
        <Container>
          <Row className="d-flex justify-content-center align-items-center">
            <Col sm>
              <BoldTitle fontWeight="700" color="#001900" fontSize="18px">
                Make your online shop with mobile app
              </BoldTitle>
              <Primary>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Primary>
              <div className="d-flex mt-4">
                <CustomButton
                  background="#000"
                  color="#fff"
                  borderRadius="8px"
                  className="me-4 d-flex align-items-center"
                >
                  <PlayStore />
                  <div className="ms-3">
                    <BoldTitle color="#fff" className="m-0">
                      Get it On
                    </BoldTitle>
                    <SubTitle color="#fff">Playstore</SubTitle>
                  </div>
                </CustomButton>
                <CustomButton
                  background="#000"
                  color="#fff"
                  borderRadius="8px"
                  className="d-flex align-items-center"
                >
                  <AppStore />
                  <div className="ms-3">
                    <BoldTitle color="#fff" className="m-0">
                      Get it On
                    </BoldTitle>
                    <SubTitle color="#fff">Appstore</SubTitle>
                  </div>
                </CustomButton>
              </div>
            </Col>
            <Col
              sm
              className="d-flex justify-content-center align-items-center"
            >
              <img
                src={require("../assets/images/iphone.png")}
                alt=""
                height="350px"
              />
            </Col>
          </Row>
        </Container>
      </div>

      <div className="text-center mt-5">
        <BoldTitle color="#4a0015">Collection</BoldTitle>
        <SubTitle>Choose Your food from Our Category</SubTitle>
      </div>
      <Container className="py-5">
        <Row className="g-4">
          {FoodItems.map((food) => (
            <Col sm style={{ minWidth: "320px", maxWidth: "320px" }}>
              <div
                style={{
                  backgroundImage: `url(${food.image})`,
                  backgroundRepeat: "no-repeat",
                  height: "260px",
                  backgroundSize: "cover",
                  borderRadius: "16px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "end",
                }}
              >
                <div
                  style={{
                    background: "#fee5ec",
                    width: "90%",
                    borderRadius: "12px",
                  }}
                  className="p-2 mb-3"
                >
                  <SubTitle>{food.popularity}</SubTitle>
                  <SubTitle className="mt-2" color="#31000e">
                    {food.name}
                  </SubTitle>
                  <SubTitle className="mt-2">
                    RS.
                    {food.price}
                  </SubTitle>
                </div>
              </div>
            </Col>
          ))}
        </Row>
        <div className="mt-4 d-flex justify-content-center">
          <CustomButton borderRadius="8px">
            <SubTitle className="m-0">See All</SubTitle>
          </CustomButton>
        </div>
      </Container>
      <div className="text-center mt-5">
        <BoldTitle color="#4a0015">Our Strength</BoldTitle>
        <SubTitle>Choose Your food from Our Category</SubTitle>
      </div>
      <Container className="py-5">
        <OurStrength />
      </Container>
      <div className="text-center mt-5">
        <BoldTitle color="#4a0015">Blogs</BoldTitle>
        <SubTitle>Choose Your food from Our Category</SubTitle>
      </div>
      <Blogs />

      <div className="py-5" style={{ background: "#ffe084" }}>
        <Features />
      </div>
      <div className="py-5">
        <Container>
          <Video />
        </Container>
      </div>
    </>
  );
};

const BestSelling = () => (
  <Row className="g-4">
    {FoodItems.slice(0, 6).map((food) => (
      <Col sm style={{ minWidth: "330px", maxWidth: "330px" }}>
        <div
          style={{
            borderRadius: " 0px 40px",
            overflow: "hidden",
            width: "fit-content",
            height: "320px",
            position: "relative",
            border: "1.5px solid #fa4e7d",
          }}
        >
          <img src={food.image} alt="" width="100%" height="200px" />
          <SkewDiv className="py-2 px-4 ">
            <SubTitle>
              <Star />
              {food.rating}
            </SubTitle>
            <EllipsisText
              color="#001900"
              fontWeigth="600"
              fontSize="18px"
              width="240px"
            >
              {food.detail}
            </EllipsisText>
            <SubTitle className="mt-2">
              RS.
              {food.price}
            </SubTitle>
            <div
              className="d-flex justify-content-center align-items-center "
              style={{
                width: "50px ",
                height: "50px",
                borderRadius: "50px",
                background: "#fa4e7d",
                position: "absolute",
                top: "40px",
                right: "20px",
              }}
            >
              <Cart />
            </div>
          </SkewDiv>
        </div>
      </Col>
    ))}
  </Row>
);

export const Features = () => (
  <Container>
    <Row className="d-flex justify-content-between ">
      <Col sm={2} style={{ minWidth: "200px" }}>
        <div className="d-flex justify-content-center align-items-center flex-column text-center">
          <FeatureBox>
            <DiscountCard />
          </FeatureBox>
          <div className="mt-2">
            <SubTitle>Discount</SubTitle>
            <Primary>Every Week new offer</Primary>
          </div>
        </div>
      </Col>
      <Col sm={2} style={{ minWidth: "200px" }}>
        <div className="d-flex justify-content-center align-items-center flex-column text-center">
          <FeatureBox>
            <DiscountCard />
          </FeatureBox>
          <div className="mt-2">
            <SubTitle>Discount</SubTitle>
            <Primary>Every Week new offer</Primary>
          </div>
        </div>
      </Col>
      <Col sm={2} style={{ minWidth: "200px" }}>
        <div className="d-flex justify-content-center align-items-center flex-column text-center">
          <FeatureBox>
            <DiscountCard />
          </FeatureBox>
          <div className="mt-2">
            <SubTitle>Discount</SubTitle>
            <Primary>Every Week new offer</Primary>
          </div>
        </div>
      </Col>
      <Col sm={2} style={{ minWidth: "200px" }}>
        <div className="d-flex justify-content-center align-items-center flex-column text-center">
          <FeatureBox>
            <DiscountCard />
          </FeatureBox>
          <div className="mt-2">
            <SubTitle>Discount</SubTitle>
            <Primary>Every Week new offer</Primary>
          </div>
        </div>
      </Col>
      <Col sm={2} style={{ minWidth: "200px" }}>
        <div className="d-flex justify-content-center align-items-center flex-column text-center">
          <FeatureBox>
            <DiscountCard />
          </FeatureBox>
          <div className="mt-2">
            <SubTitle>Discount</SubTitle>
            <Primary>Every Week new offer</Primary>
          </div>
        </div>
      </Col>
    </Row>
  </Container>
);
