import { Col, Row } from "react-bootstrap";
import { SubTitle } from "./StyleComponent";

const strengths = [
  {
    id: "1",
    type: "fresh food",
    image: require("../assets/images/freshFood.png"),
  },
  {
    id: "2",
    type: "gift",
    image: require("../assets/images/gift.png"),
  },
  {
    id: "3",
    type: "deliver",
    image: require("../assets/images/deliver.png"),
  },
  {
    id: "4",
    type: "sanitizer",
    image: require("../assets/images/sanitizer.png"),
  },
  {
    id: "5",
    type: "service",
    image: require("../assets/images/service.png"),
  },
  {
    id: "1",
    type: "fresh food",
    image: require("../assets/images/freshFood.png"),
  },
  {
    id: "1",
    type: "fresh food",
    image: require("../assets/images/freshFood.png"),
  },
  {
    id: "1",
    type: "fresh food",
    image: require("../assets/images/freshFood.png"),
  },
  {
    id: "2",
    type: "gift",
    image: require("../assets/images/gift.png"),
  },
  {
    id: "3",
    type: "deliver",
    image: require("../assets/images/deliver.png"),
  },
  {
    id: "4",
    type: "sanitizer",
    image: require("../assets/images/sanitizer.png"),
  },
  {
    id: "5",
    type: "service",
    image: require("../assets/images/service.png"),
  },
  {
    id: "1",
    type: "fresh food",
    image: require("../assets/images/freshFood.png"),
  },
  {
    id: "2",
    type: "gift",
    image: require("../assets/images/gift.png"),
  },
  {
    id: "3",
    type: "deliver",
    image: require("../assets/images/deliver.png"),
  },
  {
    id: "4",
    type: "sanitizer",
    image: require("../assets/images/sanitizer.png"),
  },
  {
    id: "5",
    type: "service",
    image: require("../assets/images/service.png"),
  },
];

export const OurStrength = () => (
  <div className="px-5">
    <Row className="g-4">
      {strengths.map((strength) => (
        <Col sm style={{ maxWidth: "170px", minWidth: "170px" }}>
          <div className="d-flex justify-content-center align-items-center flex-column">
            <div
              className="d-flex justify-content-center align-items-center mb-2"
              style={{
                width: "150px",
                minHeight: "150px",
                borderRadius: "12px",
                border: "2px solid #fee5ec",
              }}
            >
              <img src={strength.image} alt="" width="60px" height="60px"></img>
            </div>
            <SubTitle className="mt-2" color="#001900">
              {strength.type}
            </SubTitle>
          </div>
        </Col>
      ))}
    </Row>
  </div>
);
