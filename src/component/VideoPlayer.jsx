import { useEffect, useReducer, useRef } from "react";

const VIDEO_STATE = {
  STOPPED: "STOPPED",
  PLAYED: "PLAYED",
  PAUSED: "PAUSED",
};

const videoLink =
  "https://github.com/bradtraversy/vanillawebprojects/blob/master/custom-video-player/videos/gone.mp4?raw=true";

export function Video() {
  const videoRef = useRef();
  const [{ videoState, currentTime }, dispatch] = useReducer(
    (state, action) => {
      switch (action.type) {
        case "TOGGLE_PLAY_STATE": {
          const videoState =
            state.videoState === VIDEO_STATE.PLAYED
              ? VIDEO_STATE.PAUSED
              : VIDEO_STATE.PLAYED;
          return {
            ...state,
            videoState,
          };
        }
        case "STOP_VIDEO": {
          return {
            ...state,
            videoState: VIDEO_STATE.STOPPED,
            currentTime: 0,
          };
        }
        case "UPDATE_TIME": {
          return {
            ...state,
            currentTime: action.currentTime,
          };
        }
        default: {
          throw Error();
        }
      }
    },
    {
      videoState: VIDEO_STATE.STOPPED,
      currentTime: 0,
    }
  );

  /**
   * actions
   */
  function toggleVideoStatus() {
    dispatch({ type: "TOGGLE_PLAY_STATE" });
  }

  function updateTime() {
    dispatch({
      type: "UPDATE_TIME",
      currentTime: videoRef.current.currentTime,
    });
  }

  /**
   * effects
   */
  useEffect(() => {
    if (videoState === VIDEO_STATE.PLAYED) {
      videoRef.current.play();
    } else if (videoState === VIDEO_STATE.PAUSED) {
      videoRef.current.pause();
    } else if (videoState === VIDEO_STATE.STOPPED) {
      videoRef.current.currentTime = 0;
      videoRef.current.pause();
    }
  }, [videoState]);

  useEffect(() => {
    if (Math.abs(videoRef.current.currentTime - currentTime) > 0.5) {
      videoRef.current.currentTime = currentTime;
    }
  }, [currentTime]);

  useEffect(() => {
    if (currentTime === videoRef.current.duration) {
      dispatch({ type: "STOP_VIDEO" });
    }
  }, [currentTime]);

  return (
    <div
      style={{
        border: "12px solid red",
        borderRadius: "12px",
        position: "relative",
        height: "350px",
      }}
    >
      <video
        ref={videoRef}
        src={videoLink}
        onClick={toggleVideoStatus}
        onTimeUpdate={updateTime}
        height="326px"
        style={{ objectFit: "cover", position: "absolute" }}
        width="100%"
        controls
      />
    </div>
  );
}
