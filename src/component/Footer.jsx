import { Col, Container, Row } from "react-bootstrap";
import { Facebook } from "../assets/Icon";
import { BoldTitle, Primary } from "./StyleComponent";

export const Footer = () => (
  <div style={{ background: "green" }} className="pt-5">
    <Container>
      <Row className="mb-4">
        <Col
          sm={4}
          className="pe-md-5"
          style={{ minWidth: "320px", borderRight: "1px solid #fff" }}
        >
          <BoldTitle color="#fff"> Food On Ways</BoldTitle>
          <Primary color="#fff">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took
          </Primary>
        </Col>
        <Col
          sm={3}
          style={{ minWidth: "200px", borderRight: "1px solid #fff" }}
          className="d-flex justify-content-center"
        >
          <div>
            <BoldTitle color="#fff">Account</BoldTitle>
            <Primary color="#fff">Manage Account</Primary>
            <Primary color="#fff">Help</Primary>
            <Primary color="#fff">Manage Account</Primary>
          </div>
        </Col>
        <Col
          sm={3}
          style={{ minWidth: "200px", borderRight: "1px solid #fff" }}
          className="d-flex justify-content-center"
        >
          <div>
            <BoldTitle color="#fff">About</BoldTitle>
            <Primary color="#fff">Manage Account</Primary>
            <Primary color="#fff">Help</Primary>
            <Primary color="#fff">Manage Account</Primary>
          </div>
        </Col>
        <Col
          sm={2}
          style={{ minWidth: "200px" }}
          className="d-flex justify-content-center"
        >
          <div>
            <BoldTitle color="#fff">Resources</BoldTitle>
            <Primary color="#fff">Manage Account</Primary>
            <Primary color="#fff">Help</Primary>
            <Primary color="#fff">Manage Account</Primary>
          </div>
        </Col>
      </Row>
      <div className="d-flex py-4" style={{ borderTop: "1px solid #fff" }}>
        <Facebook />
        <Facebook /> <Facebook /> <Facebook />
      </div>
      <div
        className="d-flex py-4 justify-content-between"
        style={{ borderTop: "1px solid #fff" }}
      >
        <Primary color="#fff">Copyright © 2022</Primary>
        <div className="d-flex ">
          <Primary color="#fff">Terms</Primary>
          <Primary color="#fff" className="mx-3">
            Privacy
          </Primary>
          <Primary color="#fff">Terms</Primary>
        </div>
      </div>
    </Container>
  </div>
);
